#ifndef __K__HEADER__
#define __K__HEADER__

namespace K {
    template <typename T> T sum(const T& a, const T& b) {
        return a+b;
    }

    template <typename T> T product(const T& a, const T& b) {
        return a*b;
    }
}

#endif
